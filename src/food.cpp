#include <iostream>

#include "food.h"

Food::Food() : pos() {

}

void Food::reposition() {
    this->pos.x = std::rand() % FIELD_WIDTH;
    this->pos.y = std::rand() % FIELD_HEIGHT;
}

point_t Food::get_pos() {
    return pos;
}

bool Food::operator==(const point_t &pos2) {
    return pos.x == pos2.x && pos.y == pos2.y;
}
