#ifndef SNAKE_FOOD_H
#define SNAKE_FOOD_H

#include "global.h"

class Food {
private:
    point_t pos;
public:
    Food();

    void reposition();

    point_t get_pos();

    bool operator==(const point_t &pos2);
};


#endif //SNAKE_FOOD_H
