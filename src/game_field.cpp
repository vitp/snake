#include "game_field.h"

void GameField::clear_field() {
    for (auto &i : field) {
        for (char &j : i) {
            j = FIELD_CHAR;
        }
    }
}


void GameField::draw_walls() {
    change_color(color.blue);
    for (int i = 0; i < WALL_WIDTH; i++) {
        // top wall
        print(i + MARGIN, MARGIN, WALL_CHAR);
        // bottom wall
        print(i + MARGIN, WALL_HEIGHT, WALL_CHAR);
    }
    for (int i = 0; i < WALL_HEIGHT - 1; i++) {
        // right wall
        print(MARGIN, i + MARGIN, WALL_CHAR);
        // left wall
        print(MARGIN + WALL_WIDTH, i + MARGIN, WALL_CHAR);
    }
    // change color back to white
    change_color(color.white);
}


GameField::GameField() : field() {
    this->clear_field();
}

