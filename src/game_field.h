#include "global.h"

#ifndef SNAKE_GAME_FIELD_H
#define SNAKE_GAME_FIELD_H

class GameField {
public:
    char field[FIELD_WIDTH][FIELD_HEIGHT];

    void clear_field();

    void draw_walls();

    GameField();
};


#endif //SNAKE_GAME_FIELD_H
