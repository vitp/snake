#ifndef SNAKE_CONSTANTS_H
#define SNAKE_CONSTANTS_H

#include "../include/libwinapi_ncurses/src/color.h"
#include "../include/libwinapi_ncurses/src/wrapper.h"

#define MARGIN 2
//term_size ts = get_term_size();
//const int WALL_WIDTH = ts.width;
//const int WALL_HEIGHT = ts.height - 2;
//const int FIELD_WIDTH = WALL_WIDTH - 2;
//const int FIELD_HEIGHT = WALL_HEIGHT - 2 ;
#define FIELD_WIDTH 60
#define FIELD_HEIGHT 20
#define WALL_WIDTH 61
#define WALL_HEIGHT 23
#define FIELD_CHAR ' '
#define WALL_CHAR "#"
#define HEAD_CHAR 'S'
#define BODY_CHAR 'S'
#define FOOD_CHAR 'F'
#define REFRESH_RATE 80

typedef struct {
    int x, y;
} point_t;
const Color color = Color();
enum Direction {
    up, down, left, right
};
enum Status {
    PLAYING,
    WIN,
    GAME_OVER
};
#endif //SNAKE_CONSTANTS_H
