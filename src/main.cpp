#include <iostream>
#include <ctime>
#include "../include/libwinapi_ncurses/src/wrapper.h"
#include "menu.h"
#include "global.h"
#include "../include/libwinapi_ncurses/src/keys.h"
#include "snake.h"


void game_loop();

int main() {
    std::srand(std::time(nullptr));
    screen_setup();

    int v = show_menu();
    clear_screen();
    if (v == 0) {
        game_loop();
    } else if (v == 1) {
        clear_screen();
        print(2, FIELD_HEIGHT / 2, "Impostazioni non ancora implementate, premi un tasto per tornare al gioco.");
        std::cin.get();

        clear_screen();
        game_loop();
    }

    // pause then exit
    std::cin.get();
    close_window();
    return 0;
}

void game_loop() {
    Snake snake = Snake(5);
    auto gameField = snake.draw();
    gameField.draw_walls();
    snake.printToScreen(gameField);

    bool running = true;
    while (running) {
        switch (input()) {
            case ARROW_UP:
                snake.set_direction(Direction::up);
                break;
            case ARROW_LEFT:
                snake.set_direction(Direction::left);
                break;
            case ARROW_RIGHT:
                snake.set_direction(Direction::right);
                break;
            case ARROW_DOWN:
                snake.set_direction(Direction::down);
                break;
            case 'q':
            case 'Q':
            case ESC_KEY:
                running = false;
                break;
            default:
                break;
        }
        if (!running) break;
        switch (snake.move_snake(gameField)) {
            case PLAYING:
                break;
            case WIN:
                running = false;
                break;
            case GAME_OVER:
                gameField.clear_field();
                snake.printToScreen(gameField);
                std::string str = "Hai perso con " + std::to_string(snake.length) + " punti. rip.";
                print(static_cast<int>(FIELD_WIDTH / 2 - str.size() / 2), FIELD_HEIGHT / 2, str.c_str());
                running = false;
                break;
        }
        if (running) {
            print(2, 1, ("Punti : " + std::to_string(snake.length)).c_str());
            gameField = snake.draw();
            snake.printToScreen(gameField);
            _sleep(REFRESH_RATE);
        }
    }
}
