#include <iostream>
#include <sstream>

#include "../include/libwinapi_ncurses/src/wrapper.h"
#include "../include/libwinapi_ncurses/src/keys.h"
#include "global.h"
#include "menu.h"

void print_frame(term_size ts) {
    for (int i = 0; i < ts.height; ++i) {
        print(0, i, WALL_CHAR);
        print(ts.width - 1, i, WALL_CHAR);
    }
    for (int j = 0; j < ts.width; ++j) {
        print(j, 0, WALL_CHAR);
        print(j, ts.height - 1, WALL_CHAR);
    }
}

void print_banner(term_size ts) {
    std::stringstream _banner("  ____              _        \n"
                              " / ___| _ __   __ _| | _____ \n"
                              " \\___ \\| '_ \\ / _` | |/ / _ \\\n"
                              "  ___) | | | | (_| |   <  __/\n"
                              " |____/|_| |_|\\__,_|_|\\_\\___|\n");

    std::string _line;
    int y = 2, x = 0;
    while (std::getline(_banner, _line, '\n')) {
        if (x == 0) x = static_cast<int>(ts.width / 2 - _line.length() / 2);
        print(x, y++, _line.c_str());
    }
}

void print_menu(term_size ts) {
    clear_screen();
    print_frame(ts);
    print_banner(ts);
    print(static_cast<int>(ts.width / 2 - play.length() / 2), ts.height / 2, play.c_str());
    print(static_cast<int>(ts.width / 2 - settings.length() / 2), ts.height / 2 + 1, settings.c_str());
}

int show_menu() {
    term_size ts = get_term_size();
    bool running = true;
    int selection = 0;
    while (running) {
        print_menu(ts);
        switch (input()) {
            case -1:
                break;
            case ARROW_DOWN:
                selection = 1; // settings
                break;
            case ARROW_UP:
                selection = 0; // play
                break;
            case ' ': // space to select
                running = false;
                break;
            default:
                break;
        }

        print(static_cast<int>(ts.width / 2 - (selection == 0 ? play.length() : settings.length()) / 2) - 4,
              ts.height / 2 + selection, ">>");
        print(static_cast<int>(ts.width / 2 + (selection == 0 ? play.length() : settings.length()) / 2) + 2,
              ts.height / 2 + selection, "<<");
        _sleep(100); // a little bit of lag
    }
    return selection; // 0 for play 1 for settings
}