#include "../include/libwinapi_ncurses/src/wrapper.h"

#ifndef SNAKE_MENU_H
#define SNAKE_MENU_H

void print_frame(term_size ts);

void print_banner(term_size ts);

void print_menu(term_size ts);

int show_menu();

const std::string play = "PLAY";
const std::string settings = "SETTINGS";
#endif