#include "snake.h"

#include "../include/libwinapi_ncurses/src/wrapper.cpp"

Snake::Snake(int initialLength) : snake(), length(initialLength), speed(1), dir(Direction::left), food() {
    food.reposition();
    for (int i = 0; i < length; ++i) {
        snake[i].x = i + FIELD_WIDTH / 2 + MARGIN + 1;
        snake[i].y = FIELD_HEIGHT / 2 + MARGIN + 1;
    }
}

void Snake::set_direction(Direction direction) {
    if (opposite(direction, dir))
        return;
    this->dir = direction;
}

bool Snake::opposite(Direction d1, Direction d2) {
    return (d1 == Direction::left && d2 == Direction::right) || (d1 == Direction::right && d2 == Direction::left) ||
           (d1 == Direction::up && d2 == Direction::down) || (d1 == Direction::down && d2 == Direction::up);
}

Status Snake::move_snake(const GameField &gameField) {
    point_t head = snake[0];
    switch (dir) {
        case up:
            head.y -= 1;
            break;
        case down:
            head.y += 1;
            break;
        case left:
            head.x -= 1;
            break;
        case right:
            head.x += 1;
            break;
    }

    if (gameField.field[head.x][head.y] == BODY_CHAR) return GAME_OVER;

    for (int i = length - 1; i > 0; --i) {
        snake[i].x = snake[i - 1].x;
        snake[i].y = snake[i - 1].y;
    }

    if (food == head) {
        snake[length].x = snake[length - 1].x;
        snake[length].y = snake[length - 1].y;

        length++;
        food.reposition();

        if (length == Snake::snake_max_size)
            return WIN;
    }

    if (head.x > FIELD_WIDTH || head.x < 0 || head.y < 0 || head.y >= FIELD_HEIGHT) {
        return GAME_OVER;
    }

    snake[0] = head;
    return PLAYING;
}

GameField Snake::draw() {
    GameField newField = GameField();
    newField.field[snake[0].x][snake[0].y] = HEAD_CHAR;
    for (int i = 1; i < length; ++i) {
        newField.field[snake[i].x][snake[i].y] = BODY_CHAR;
    }

    newField.field[food.get_pos().x][food.get_pos().y] = FOOD_CHAR;
    return newField;
}

void Snake::printToScreen(const GameField &gameField) {
    change_color(color.green);
    for (int i = 0; i < FIELD_WIDTH; ++i) {
        for (int j = 0; j < FIELD_HEIGHT; ++j) {
            if (gameField.field[i][j] == FOOD_CHAR) change_color(color.red);
            print(i + MARGIN + 1, j + MARGIN + 1, gameField.field[i][j]);
            if (gameField.field[i][j] == FOOD_CHAR) change_color(color.green); // set color back to green
        }
    }
    change_color(color.white);

}
