#ifndef SNAKE_SNAKE_H
#define SNAKE_SNAKE_H

#include "../include/libwinapi_ncurses/src/color.h"
#include "global.h"
#include "food.h"
#include "game_field.h"

class Snake {
private:
    static const int snake_max_size = 300;
    point_t snake[snake_max_size];
    int speed;
    Direction dir;
    Food food;
public:
    int length;

    Snake(int initialLength);

    void set_direction(Direction direction);

    bool opposite(Direction d1, Direction d2);

    Status move_snake(const GameField &gameField);

    GameField draw();

    void printToScreen(const GameField &gameField);
};

#endif //SNAKE_SNAKE_H
